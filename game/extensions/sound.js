
class GameSound {
    eat() {}
}


export class WebAudioGameSound extends GameSound {
    constructor({eatSoundSrc}) {
        super()
        this._eatSound = new Audio(eatSoundSrc)
    }

    eat() {
        this._eatSound.currentTime = 0
        this._eatSound.play()
    }   
}