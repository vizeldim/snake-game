export const Dir = {
    RIGHT: 1,
    DOWN: 2,
    LEFT: 3,
    UP: 4 ,
    NONE: 5
}

export const getDir = (from, to) => {
    const diffX = to.x - from.x
    const diffY = to.y - from.y

    if(diffX === 0 && diffY === 0) {
        return Dir.NONE
    }
    
    if(diffY === 0){
        return (diffX > 0) ? Dir.RIGHT : Dir.LEFT
    }

    return (diffY > 0) ? Dir.DOWN : Dir.UP
}