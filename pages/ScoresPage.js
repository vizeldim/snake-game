import Page from './template/pageTemplates.js';
import { createBasePageElem,createCardElem,createCardListElem,createClipartElem,createToggleSwitch } from '../elements/htmlContentFactory.js';

export class ScoresPage extends Page {
    constructor(id, services) {
        super(id, services)
        
        this._service = services.scoresService
        this._scores = {}

        const {content, wrapper} = createBasePageElem('snake game')
                this._elem = content

        const clipart = createClipartElem('./img/snake-clipart.png')
        wrapper.append(this.scoresCard,  clipart)

        this.init()         
    }

    init() {
        this._service
            .getScores()
            .then(s => {
                this._scores = s
                this._renderScores()  
            })   
    }

    _renderScores() {
        this._scoresE.innerHTML = ''

        Object.values(this._scores).forEach(({map, score})=>{
            const dt = document.createElement('dt')
            dt.textContent = map
            dt.classList.add('card-map')

            const dd = document.createElement('dd')
            dd.textContent = score
            dd.classList.add('card-val')

            this._scoresE.append(dt, dd)
        })
    }
    
    get scoresCard() {
        const innerContent = document.createElement('section')
        innerContent.classList.add('inner-content')
      
        const dl = document.createElement('dl')
        dl.classList.add('card-list')
        this._scoresE = dl

        const [card, back] = createCardElem('scores', dl)
        innerContent.appendChild(card)

        back.addEventListener('click', (e) => {
            e.preventDefault()
            this._router.navigate('main')
        })

        return innerContent
    }

    get htmlContent () {
        return this._elem
    }
}