import Page from "./template/pageTemplates.js";
import { createBasePageElem, createNavElem, createClipartElem, createSvgSnakeAnimationElem } from '../elements/htmlContentFactory.js';

export class MainMenuPage extends Page {
    constructor(id, services) {
        super(id, services)

        const {content, wrapper} = createBasePageElem("snake game")

        const lst = [
            {title: 'Play', page: 'game'}, 
            {title: 'Scores', page: 'scores'}, 
            {title: 'Settings', page: 'settings'}]
            
        const nav = createNavElem(this._router, lst)
        const svgAnim= createSvgSnakeAnimationElem()
        const clipart = createClipartElem('./img/snake-clipart.png')

        wrapper.append(nav, svgAnim, clipart)

        this._elem = content
    }
}