
class GameMapDataSource {
    constructor() {
        this._maps = [
            {
                id: '',
                name: '',
                content: ''
            }
        ]
    }

    find(mapId) {}

    list() { return [] }
}

export class InMemoryGameMapDataSource extends GameMapDataSource {
    constructor(mapsArr) {
        super()

        this._maps = mapsArr
        this._list = mapsArr.map(
            m => {return {'id': m.id, 'name': m.name}}
        )
    }

    find(mapId) {
        return Promise.resolve(this._maps.find(m => m.id === mapId))
    }

    list() {
        return [...this._list]
    }
}
