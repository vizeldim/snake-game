class ScoresDataSource {
    constructor() {
        this._scores = {} // {mapX: 13}
        this._loaded = false
    }

    async getScores() {
        return Promise.resolve({...this._scores})
    }

    async saveScores(newScores) {
        this._scores = {...newScores}
        return Promise.resolve(true)
    }

    async saveScore(mapId, mapName, score) {
        if(!this._loaded) {
            return this
                .getScores()
                .then(s => {
                    this._scores = s
                    this._scores[mapId] = {map: mapName, score}
                    this.saveScores(this._scores)
                })
        }

        this._scores[mapId] = {map: mapName, score}
        return this.saveScores(this._scores)
    }

    async getScore(mapId) {
        return Promise.resolve(this._scores[mapId])
    }
}


export class LocalStorageScoresDataSource extends ScoresDataSource {
    constructor(localStorageName) {
        super()

        this._localStorageName = localStorageName

        // this.saveScores({
        //     '0': {map: 'Map No Walls', score: 13}
        // })
    }

    _loadScores() {
        let strScores = localStorage.getItem(this._localStorageName)
        let objScores = null

        if(strScores) {
            try {
                const parsedScores = JSON.parse(strScores) 
                objScores = parsedScores
            } catch(e) {
                console.log("INVALID SCORES IN MEMORY => empty scores")
                strScores = false
            }
        }
        
        if(!strScores) {
            this.saveScores({})
        }
        else {
            this._scores = {...objScores} 
        }

        this._loaded = true
    }

    async saveScores(newScores) {
        super.saveScores(newScores)

        const strScores = JSON.stringify(newScores)
        localStorage.setItem(this._localStorageName, strScores)
        return Promise.resolve(true)
    }

    async getScores() {
        if(!(this._loaded)) {
            this._loadScores()
        }

        return Promise.resolve({...this._scores})
    } 
}
