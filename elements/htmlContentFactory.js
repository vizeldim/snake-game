
export const createHorizontalHeaderHTML = (title) => {
    return `<header class="title-header">
                <h1 class="vertical-title">${title}</h1>
            </header>`
}

export const createSvgSnakeAnimationElem = () => {
    const xmlns = 'http://www.w3.org/2000/svg'
    const content = document.createElement('div')
    content.classList.add('snake-svg-container')

    const svg = document.createElementNS(xmlns, 'svg')

    const lens = [
        2, 3, 6, 4 ,4
    ]
    const SIZE = 40
    const space = 40 + SIZE

    const total = lens.reduce((prev, cur) => prev + cur)
    let cur = 0

    const svgLine = (horizontal, x, y, len, svg) => {
        for (let i = 0; i < len; i++) {
            const r = document.createElementNS(xmlns, 'rect')

            r.setAttributeNS(null, 'x', x + (horizontal ? space * i : 0))
            r.setAttributeNS(null, 'y', y + (horizontal ? 0 : space * i ))
            r.setAttributeNS(null, 'width', SIZE)
            r.setAttributeNS(null, 'height', SIZE)
            r.style=`--part: ${total - (cur++)}`
            svg.appendChild(r)
        }
    }

    let pos = {x: 20, y: -20}
    for (let i = 0; i < 5; ++i) {
        const l =  lens[i]
        svgLine((i % 2), pos.x, pos.y,l, svg) 

        pos.x += ((i % 2) ? (l * space ) : 0)
        pos.y += ((i % 2) ? 0 : (l * space))
    }

    content.appendChild(svg)

    return content
//width="366" height="444" rx="5" transform="translate(1279.115 314.613) rotate(30)" fill="#525252"
}

export const createFullScreenBtn = () => {
    const fullScreenToggler = () => {
        if (!document.fullscreenElement) {
            document.body.requestFullscreen();
        } 
        else if (document.exitFullscreen) {
            document.exitFullscreen();
        }
    }

    const btn = document.createElement('button')
    btn.classList.add('fullscreen-btn')
    btn.addEventListener('click', fullScreenToggler)

    return btn
}

export const createBasePageElem = (title) => {
    const {content, wrapper} = createEmptyPageElem()

    wrapper.innerHTML = createHorizontalHeaderHTML(title)

    return {content, wrapper}
}

export const createEmptyPageElem = () => {
    const content = document.createElement('div')
    content.classList.add('content')

    const inner = document.createElement('inner')
    inner.classList.add('inner')

    const btn = createFullScreenBtn()

    const wrapper = document.createElement('div')
    wrapper.classList.add('wrapper')

    inner.append(btn, wrapper)
    content.append(inner)

    return {content, wrapper}
}

export const createLink = (router, title, page, cb) => {
    const linkHandler = (e) => {
        e.preventDefault()
        if(cb) { cb() } 
        router.navigate(page)
    }

    const a = document.createElement('a')
    a.textContent = title
    a.href = '#'
    
    a.addEventListener('click', linkHandler)

    return a
}

// [{title: 'xxx', page: 'xxx'}, ...]
export const createNavElem = (router, linkArr) => {
    const nav = document.createElement('nav')
    nav.classList.add('main-nav')
    
    const ul = document.createElement('ul')
   
    linkArr.forEach(i => {
        const li = document.createElement('li')
        const a = createLink(router, i.title, i.page)
        li.append(a)
        ul.append(li)
    })

    nav.append(ul)

    return nav
}


// [{term: 'string', def: HtmlElement}, ...]
export const createCardListElem = (lst) => {
    const menuList = document.createElement('dl')
    menuList.classList.add('card-list')

    lst.forEach(i => {
        const dt = document.createElement('dt')
        dt.textContent = i.term

        const dd = document.createElement('dd')
        dd.append(i.def)

        menuList.append(dt, dd)
    })

    return menuList
}




/*
    <div class="clipart">
        <img src=" {imgSrc} " alt="snake-clipart">
    </div>`
*/
export const createClipartElem = (imgSrc) => {
    const div = document.createElement('div')
    div.classList.add('clipart')
    div.innerHTML = `<img src="${imgSrc}" alt="snake-clipart">`

    return div
}


/*
    <label class="switch">
        <input class="slider" type="checkbox">
        <span class="slider"></span>
    </label>
*/
export const createToggleSwitch = (isChecked, cb) => {
    const label = document.createElement('label') 
    label.classList.add('switch')

    const input = document.createElement('input')
    input.classList.add('slider')
    input.type = 'checkbox'
    input.checked = isChecked ?? false
    input.addEventListener('change', (e) => {
        if(e.target.checked) {
            cb(true)
        } else {
            cb(false)
        }
    })

    const span = document.createElement('span')
    span.classList.add('slider')
  
    label.append(input, span)
    return label
}



/*
 <article class="card scores-card">
    <div class="card-bar">
        <a class="back-arrow" href="#"></a>
        <h2 class="scores-title"> {title} </h2>
    </div>
    <div class="card-content">
        { content }
    </div>
</article>
*/
export const createCardElem = (title, content) => {  
    const card = document.createElement('article')
    card.classList.add('card')

    const cardBar = document.createElement('div')
    cardBar.classList.add('card-bar')

    const backLink = document.createElement('a')
    backLink.href = '/'
    backLink.classList.add('back-arrow')
    
    const cardTitle = document.createElement('h2')
    cardTitle.classList.add('card-title')
    cardTitle.textContent = title
    cardBar.append(backLink, cardTitle)

    const cardContent = document.createElement('div')
    cardContent.classList.add('card-content')
    cardContent.appendChild(content)

    cardBar.append(backLink, cardTitle)

    card.append(cardBar, cardContent)

    return [card, backLink]
}